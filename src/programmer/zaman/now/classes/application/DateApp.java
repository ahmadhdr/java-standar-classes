package programmer.zaman.now.classes.application;
import java.util.Date;
public class DateApp {
    public static void main(String[] args) {
        /**
         * untuk mengubah tanggal harus menggunakan mili second di constructor nya
         * makanya untuk sekangkan di rekomendasikan mengguanakn calendar class
         */
        Date tanggal = new Date();
        System.out.println(tanggal);
    }
}
