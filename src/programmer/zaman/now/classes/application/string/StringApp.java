package programmer.zaman.now.classes.application.string;

public class StringApp {
    public static void main(String[] args) {
        /**
         * method toLowerCase pada java mereturn string yang baru dan tidak merubah
         * string yang ada.
         * method toUppercase adalah lawan
         * length digunakan untuk menghitung jumlah karakter
         * starWith di gunakan untuk mengecek apakah karakter pertama pada sebuah kata atau kalimat
         * sama dengan parameter yang diberikan
         * endWith adalah kebalikan dari startWith
         * split di gunakan untuk memecah string
         * isblank mengecek apakah sebuah string kalaupun ada spasi maka di sebut blank
         * isEmpty spasi
         *
         * String di java adalah immutable, ketika kita mutate string
         * sebenernya java membuat memory baru
         */
        var asd = "haidar";
        String name = "Ahmad haidar albaqir";
        String toLowercaseName = name.toLowerCase();
        String toUppercase = name.toUpperCase();
        int lengthString = name.length();

        System.out.println(name);
        System.out.println(toUppercase);
        System.out.println(toLowercaseName);
        System.out.println(lengthString);
        System.out.println(name.startsWith(("Ahmad")));
        System.out.println(name.endsWith("albaqir"));
        String[] names = name.split(" ");

        for(var value: names) {
            System.out.println("value =>" + value);
        }
        System.out.println(" ".isBlank());
        System.out.println(" ".isEmpty());
        System.out.println(name.charAt(0));

        char[] chara = name.toCharArray();

        for(var value1: chara) {
            System.out.println(value1);
        }

    }
}
