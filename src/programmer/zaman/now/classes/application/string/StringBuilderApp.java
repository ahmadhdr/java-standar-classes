package programmer.zaman.now.classes.application.string;

public class StringBuilderApp {
    public static void main(String[] args) {
        /**
         * sebeneranya membuat 3 data didalam memory,
         * sangat tidak disarankan ketika memanipulasi string yang banyak
         * menggunakan string  tapi menggunakan stringbuilder
         */
//        String name = "ahmad";
//
//        name = name  + " haidar";
//        name = name + " albaqir";

        StringBuilder builder = new StringBuilder();
        builder.append("ahmad ");
        builder.append("haidar ");
        builder.append("albaqir");

        System.out.println(builder.toString());
    }
}
