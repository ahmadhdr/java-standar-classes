package programmer.zaman.now.classes.application.string;

import java.util.StringJoiner;

public class StringJoinerApp {
    public static void main(String[] args) {
        String[] names = {"ahmad","haidar","albaqir"};
        StringJoiner joiner = new StringJoiner(
            ",",
            "[",
            "]"
        );
        for(var name: names) {
            joiner.add(name);
        }

        System.out.println(joiner.toString());
    }
}
