package programmer.zaman.now.classes.application.string;

import java.util.StringTokenizer;

public class StringTokenizerApp {
    private String name;
    public static void main(String[] args) {
        /**
         * hampir sama dengan method split di class object
         * hanya saja string tokenizer bersifat lazy dan lebih
         * hemat memeory
         */
        String value = "Ahmad,haidar,albaqir";
        StringTokenizer tokenizer = new StringTokenizer(value, ",");

        while(tokenizer.hasMoreTokens()) {
            String result = tokenizer.nextToken();
            System.out.println(result);
        }
    }
}
