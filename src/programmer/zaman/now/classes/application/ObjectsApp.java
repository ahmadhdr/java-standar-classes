package programmer.zaman.now.classes.application;

import java.util.Objects;

public class ObjectsApp {

    public static class Data {
        private String data;

        public String getData() {
            return data;
        }

        Data(String data) {
            this.data = data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Data data1 = (Data) o;
            return Objects.equals(data, data1.data);
        }

        @Override
        public int hashCode() {
            return Objects.hash(data);
        }

        public void setData(String data) {
            this.data = data;
        }
    }

    public static void main(String[] args) {
        /**
         * Object adalah class utility yang berisikan static method yang bisa digunakan
         */
        Execute(new Data(("ahad haidar")));
    }

    public static void Execute(Data data) {
        System.out.println(data.toString());
        System.out.println(data.hashCode());
    }

}
