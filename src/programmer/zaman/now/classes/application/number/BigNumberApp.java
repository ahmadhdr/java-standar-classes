package programmer.zaman.now.classes.application.number;

import java.math.BigInteger;

public class BigNumberApp {
    public static void main(String[] args) {
        /**
         * digunakan untuk menghandle angka yang kapasitasnya
         * melebihi kapasitas long
         * */
        BigInteger a = new BigInteger("10000000000000000000000");
        BigInteger b = new BigInteger("20000000000000000000000");

        BigInteger result = a.add(b);
        System.out.println(result);

    }
}
