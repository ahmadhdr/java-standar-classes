package programmer.zaman.now.classes.application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesApp {
    public static void main(String[] args) {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("application.properties"));

            String host = properties.getProperty("database.host");
            String port = properties.getProperty("database.username");
            String password = properties.getProperty("database.password");
            System.out.println(host);
            System.out.println(port);
            System.out.println(password);
        }catch(FileNotFoundException exception) {
            System.out.println("File tidak ketemeu");
        }catch (IOException exception) {
            System.out.println("Gagal load file");
        }

        try {
            Properties properties = new Properties();

            properties.put("name.first","Ahmad");
            properties.store(new FileOutputStream("name.properties"),"");

        }catch(FileNotFoundException exception) {
            System.out.println("error membuat file atau load file");
        } catch (IOException exception) {
            System.out.println("error menyimpa proerties");
        }


    }
}
