package programmer.zaman.now.classes.application;

import java.util.Calendar;
import java.util.Date;

public class CalenderApp {
    public static void main(String[] args) {
        // add di gunakan untuk menambahkan dan mengedit waktu

        /**
         * terdapat 2 cara mengubah tanggal
         * menggunakan add atau menggunakan set
         * add menambah sedangkan set mengurang
         */
        Calendar calender = Calendar.getInstance();
        calender.set(Calendar.YEAR, 2001);
        calender.add(Calendar.YEAR, -10);

        Date result = calender.getTime();
        System.out.println(result);
    }
}
