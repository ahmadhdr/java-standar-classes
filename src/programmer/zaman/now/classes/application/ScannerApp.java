package programmer.zaman.now.classes.application;

import java.util.Scanner;

public class ScannerApp {
    public static void main(String[] args) {
        Scanner tulisan = new Scanner(System.in);

        System.out.print("Name : ");
        String name = tulisan.nextLine();

        System.out.println("Nama anda adalah => " + name);

    }
}
