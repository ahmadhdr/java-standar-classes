package programmer.zaman.now.classes.application;

public class SystemApp {
    public static void main(String[] args) {
        /**
         * sebuah class yang beriskan banyak utility static method di java
         * contoh
         * getEnv untk mendapatkan environment variable sistem operasi
         * system exit menghentikan program java
         * dan masih banyak yang lainnya
         *
         */
        System.out.println(System.currentTimeMillis());
        System.out.println(System.nanoTime());
        System.exit(0);

        System.gc();

        System.out.println(System.getenv("APP"));
    }
}
